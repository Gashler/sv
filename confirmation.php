<?php
    session_start();
    $title = 'Confirmation';
?>
<?php include('header.php') ?>
<div class="page">
    <h1 style="font-size:2em;">Thank You!</h1>
    <p>One of our travel agents will contact you within a few business days with a free quote for your vacation package.</p>
    <a class="button" href="/profile.php?p=<?php echo $_SESSION['primary'] ?>&s=<?php echo $_SESSION['secondary'] ?>&l=<?php echo $_SESSION['l'] ?>">See Your Ideal Vacation <i class="fa fa-angle-right"></i></a>
    <!-- <p><small>Redirecting in <span id="counter">10</span> seconds</small></p> -->
</div>
<script>

    // countdown then redirect
    // var time = 10;
    // setInterval(function() {
    //     time --;
    //     $('#counter').html(time);
    //     if (time == 0) {
    //         window.location = '/about';
    //     }
    // }, 1000);

</script>
<?php include('emails/sale-confirmation.php') ?>
<?php include('footer.php') ?>
