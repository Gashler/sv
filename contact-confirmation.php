<?php
    session_start();
    $title = 'Message Sent';
?>
<?php include('header.php') ?>
<div class="page">
    <h1 style="font-size:2em;">Thank You for Contacting Us!</h1>
    <p>One of our travel agents will contact you within a few business days.</p>
    <p><small>Redirecting in <span id="counter">10</span> seconds</small></p>
</div>
<script>

    countdown then redirect
    var time = 10;
    setInterval(function() {
        time --;
        $('#counter').html(time);
        if (time == 0) {
            window.location = '/about';
        }
    }, 1000);

</script>
<?php include('footer.php') ?>
