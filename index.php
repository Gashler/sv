<?php
    $title = "Find Out Your Ideal Vacation";
    $image = "http://surprise-vacation.com/img/home/1.jpg";
?>
<?php include('header.php') ?>
<div ng-controller="survey" class="page-wide">
    <div class="section no-padding">
        <div class="explainer">
            <h1>Discover Your Ideal Vacation</h1>
            <h2><span class="circle">1</span> Would You Rather Spend a Day ...</h2>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/1.jpg);">
            <img class="section-image" src="/img/home/1.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">A</span>Relaxing at a beach</h3>
                <button ng-click="next(2, 'activity', 'beach')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/2.jpg);">
            <img class="section-image" src="/img/home/2.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">B</span>Hiking in the mountains</h3>
                <button ng-click="next(3, 'activity', 'hiking')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/10.jpg);">
            <img class="section-image" src="/img/home/10.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">C</span>Playing at a theme park</h3>
                <button ng-click="next(1, 'activity', 'theme park')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/11.jpg);">
            <img class="section-image" src="/img/home/11.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">D</span>Sight Seeing</h3>
                <button ng-click="next(4, 'activity', 'sight seeing')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
    </div>
    <div class="section no-padding">
        <div class="explainer">
            <h2><span class="circle">2</span> Would You Rather ...</h2>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/4.jpg);">
            <img class="section-image" src="/img/home/4.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">A</span>Experience Fine Dining</h3>
                <button ng-click="next(4, 'dining', 'fine dining')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/9.jpg);">
            <img class="section-image" src="/img/home/9.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">B</span>Discover a New Cuisine</h3>
                <button ng-click="next(3, 'dining', 'new cusine')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/12.jpg);">
            <img class="section-image" src="/img/home/12.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">C</span>Enjoy Your Favorite Comfort Food</h3>
                <button ng-click="next(2, 'dining', 'comfort food')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/13.jpg);">
            <img class="section-image" src="/img/home/13.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">D</span>Eat on a Rooftop</h3>
                <button ng-click="next(1, 'dining', 'rooftop')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
    </div>
    <div class="section no-padding">
        <div class="explainer">
            <h2><span class="circle">3</span> How Would You Rather Travel?</h2>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/19.jpg);">
            <img class="section-image" src="/img/home/19.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">A</span>By Yacht</h3>
                <button ng-click="next(4, 'travel', 'yacht')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/22.jpg);">
            <img class="section-image" src="/img/home/22.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">B</span>By Road Trip</h3>
                <button ng-click="next(1, 'travel', 'road trip')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/20.jpg);">
            <img class="section-image" src="/img/home/20.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">C</span>By Bicycle</h3>
                <button ng-click="next(3, 'travel', 'bicycle')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/21.jpg);">
            <img class="section-image" src="/img/home/21.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">D</span>By First Class Cabin</h3>
                <button ng-click="next(2, 'travel', 'first class')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
    </div>
    <div class="section no-padding">
        <div class="explainer">
            <h2><span class="circle">4</span> Would You Rather Enjoy ...</h2>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/17.jpg);">
            <img class="section-image" src="/img/home/17.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">A</span>Stand-up Comedy</h3>
                <button ng-click="next(1, 'entertainment', 'comedy')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/7.jpg);">
            <img class="section-image" src="/img/home/7.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">B</span>A Rock Concert</h3>
                <button ng-click="next(3, 'entertainment', 'rock concert')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/18.jpg);">
            <img class="section-image" src="/img/home/18.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">C</span>A Fancy Ball</h3>
                <button ng-click="next(4, 'entertainment', 'fancy ball')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/15.jpg);">
            <img class="section-image" src="/img/home/15.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">D</span>A Play</h3>
                <button ng-click="next(2, 'entertainment', 'theatre')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
    </div>
    <div class="section no-padding">
        <div class="explainer">
            <h2><span class="circle">5</span> Would You Rather Stay at ...</h2>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/5.jpg);">
            <img class="section-image" src="/img/home/5.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">A</span>A Campground</h3>
                <button ng-click="next(3, 'lodging', 'campground')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/14.jpg);">
            <img class="section-image" src="/img/home/14.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">B</span>A Fun Resort</h3>
                <button ng-click="next(1, 'lodging', 'fun resort')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/16.jpg);">
            <img class="section-image" src="/img/home/16.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">C</span>A Private Villa</h3>
                <button ng-click="next(4, 'lodging', 'private villa')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="division width-half height-half" style="background-image:url(img/home/6.jpg);">
            <img class="section-image" src="/img/home/6.jpg">
            <div class="division-content">
                <h3 class="float-left"><span class="circle">D</span>An Ellegant Bed and Breakfast Hotel</h3>
                <button ng-click="next(2, 'lodging', 'bed and breakfast')" class="float-right">Select <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="padding-5">
            <h2 class="margin-top-0">Determining Your Ideal Vacation ...</h2>
            <div class="progress-bar">
                <div class="bar"></div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
