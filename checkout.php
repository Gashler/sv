<?php
session_start();
$json = file_get_contents('pricing.json');
$pricing = json_decode($json);
if (isset($_REQUEST['package'])) {
    $package = $_REQUEST['package'];
    $_SESSION['package'] = $package;
}
if (isset($_REQUEST['price'])) {
    $price = $_REQUEST['price'];
    $_SESSION['price'] = $price;
}
$title = 'Checkout';
if (isset($_REQUEST['l'])) {
    $_SESSION['l'] = $_REQUEST['l'];
}
// store session variables
$_SESSION['primary'] = $_GET['p'];
$_SESSION['secondary'] = $_GET['s'];
$_SESSION['l'] = $_GET['l'];

$json = file_get_contents('data.json');
$data = json_decode($json);

?>
<?php include('header.php') ?>
<div class="page" ng-controller="checkout">
    <h1 class="margin-top-0" style="font-size:2em;">A Few More Questions</h1>
    <!-- <form name="form" action="https://www.paypal.com/cgi-bin/webscr" method="post" onsubmit="return validateForm()"> -->
    <form name="form" action="/confirmation" method="post" onsubmit="return validateForm()">

    <ol>
        <li>
            <!-- <p class="alert margin-top-0">
                Packages are optimized for couples and small families. For larger groups, consider upgrading your package to avoid a compromise of quality.
            </p> -->
            How many people are in your party?
            <input type="number" name="number" ng-model="data.number" ng-blur="updateSession()" placeholder="Max: 8" min="1" max="8">
        </li>
        <?php if (isset($_SESSION['profile_location'])) : ?>
            <li>
                <label>Destination:</label>
                <select name="destination" ng-model="data.destination" ng-init="data.destination = 'Send me to <?php echo $_SESSION['profile_location'] ?>'" ng-change="updateSession()">
                    <option selected>Send me to <?php echo $_SESSION['profile_location'] ?></option>
                    <option>Surprise me with somewhere else</option>
                </select>
            </li>
        <?php endif ?>
        <li>
            When would you like to leave?
            <input type="text" name="depart" ng-model="data.depart" ng-blur="updateSession()">
        </li>
        <li>
            Your City:
            <input type="text" name="city" ng-model="data.city" ng-blur="updateSession()">
        </li>
        <li>
            Your State or Country:
            <input type="text" name="state" ng-model="data.state" ng-blur="updateSession()">
        </li>
        <li>
            Please list anywhere you would NOT like to go:
            <br>
            <textarea rows="5" name="blacklist" ng-model="data.blacklist" ng-blur="updateSession()"></textarea>
        </li>
        <li>
            Your name:
            <input type="text" name="name" ng-model="data.name" ng-blur="updateSession()">
        </li>
        <li>
            Your email address:
            <small>So we can send you your free quote</small> 
            <input type="text" name="email" ng-model="data.email" ng-blur="updateSession()">
        </li>
        <!-- <li>
            Your phone number:
            <input type="text" name="phone" ng-model="data.phone" ng-blur="updateSession()">
        </li> -->
        <li>
            Any special requests or considerations:
            <textarea rows="5" ype="text" name="phone" ng-model="data.notes" ng-blur="updateSession()"></textarea>
        </li>
    </ol>
    <br>
        <!-- <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="9TUNQFTEVK9FU">
        <input type="hidden" name="os0" value="<?php echo $price ?>">
        <input type="hidden" name="currency_code" value="USD">
        <button>Checkout <i class="fa fa-angle-right"></i></button>
        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->
        <button>Next <i class="fa fa-angle-right"></i></button>
    </form>
</div>
<script src="/js/controllers/checkout.js"></script>
<script>

    // validate
    function validateForm()
    {
        var fields = [
            'destination',
            'number',
            'depart',
            'blacklist',
            'city',
            'country',
            'name',
            'email',
            // 'phone'
        ];
        for (i = 0; i < fields.length; i++) {
            var fieldname = fields[i];
            if (document.forms["form"][fieldname].value === "") {
                alert("Please complete the form before continuing.");
                return false;
            }
        }
        return true;
    }

</script>
<?php include('footer.php') ?>
