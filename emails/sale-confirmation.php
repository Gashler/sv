<?php
$json = file_get_contents('pricing.json');
$pricing = json_decode($json);
$to = "support@surprise-vacation.com";
$subject = "New Surprise Vacation Customer";
$message = "
<html>
    <head>
        <title>New Surprise Vacation Customer</title>
    </head>
    <body>
        <h1>New Surprise Vacation Customer</h1>
        <table>
";
foreach ($_SESSION as $key => $value) {
    $message .= "
            <tr>
                <th style='text-align:right;'>" . ucfirst($key) . "</th>
                <td>" . $value . "</td>
            </tr>
    ";
}
$message .= "
        </table>
    </body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type:text/html;charset=UTF-8\r\n";

// More headers
$headers .= "From: <support@surprise-vacation.com>";

mail($to, $subject, $message, $headers);
