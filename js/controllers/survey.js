angular.module('app')
.controller('survey', function($scope, $location, $http, $timeout) {

    $http.get('/data.json').then(function(response) {
        $scope.locs = response.data.locations;
        $scope.types = response.data.types;
    });

    var responses = {};
    var question_number = 1;
    var pausing;
    $('.section').hide();
    $('.section:first').show();
    $scope.next = function(type, question, answer) {
        if (pausing) {
            return;
        }

        // force pause to avoid accidentally clicking through multiple answers
        pausing = true;
        setTimeout(function() {
            pausing = false;
        }, 1000);

        if (type !== undefined) {
            $scope.types[type - 1].votes ++;
            responses[question] = answer;
        }
        $('.section').hide();
        question_number ++;
        $('.section:nth-of-type(' + question_number + ')').show();
        $(window).scrollTop(0);

        // results page
        if (question_number == 6) {

            // determine trends in answers
            $scope.types.sort(function(a, b) {
                return b.votes - a.votes;
            });

            var rand;
            var primary_type;
            var secondary_type;

            // if first two types have equal votes, randomize winner
            if ($scope.types[0].votes == $scope.types[1].votes) {
                rand = Math.floor((Math.random() * 2));
                primary_type = $scope.types[rand];
                if (rand == 0) {
                    secondary_type = $scope.types[1];
                } else {
                    secondary_type = $scope.types[0];
                }
            } else {
                primary_type = $scope.types[0];
                secondary_type = $scope.types[1];
            }

            // if first type got all votes
            if ($scope.types[1].votes == 0) {
                secondary_type = $scope.types[0];
            } else {
                secondary_type = $scope.types[1];
            }

            // get loc
            var loc_matches = [];
            angular.forEach($scope.locs, function(loc, index) {
                if (loc.primary == primary_type.id && loc.secondary == secondary_type.id) {
                    loc_matches.push(index);
                }
            });
            var rand = Math.floor((Math.random() * loc_matches.length));
            var loc = loc_matches[rand];

            // animate progress bar
            $('.progress-bar .bar').addClass('animate-5');

            // update session with answer data
            console.log('responses = ', responses);
            $http.post('/controllers/update-session', responses);

            // redirect
            $timeout(function() {
                window.location =  "/checkout.php?p=" + primary_type.id + "&s=" + secondary_type.id + "&l=" + loc;
            }, 5000);
        }

    }
});
