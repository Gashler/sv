angular.module('app')
.controller('checkout', function($scope, $location, $http) {

    // update session
    $scope.updateSession = function() {
        $http.post('/controllers/update-session', $scope.data);
    }

});
