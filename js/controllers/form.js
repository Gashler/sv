angular.module('app')
.controller('form', function($scope, $http, $location) {

    /*****************
    * Sign Up Form
    ******************/

    // submit form
    $scope.submit = function() {
        $http.post('/submit.php', $scope.form).then(function(response) {
            console.log(response);
            $location.path('/confirmation');
        });
    }

});
