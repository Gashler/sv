angular.module('app')
.controller('profile', function($scope, $location, $http) {

    // get data
    $http.get('/data.json').then(function(response) {
        $scope.locations = response.data.locations;
        $scope.data_types = response.data.types;

        // get primary and secondary types
        $scope.types = [];
        angular.forEach($scope.data_types, function(type, index) {
            if (type.id == primary) {
                $scope.types[0] = type;
            }
            if (type.id == secondary) {
                $scope.types[1] = type;
            }
        });

        // get location
        $scope.place = $scope.locations[location];
    });

});
