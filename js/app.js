// register angular app module
var app = angular.module('app', ['ngRoute'])

// routes
.config(function($routeProvider) {
    $routeProvider.

    // profile
    when('/profile', { templateUrl:'/profile.html', controller:'profile' }).

    otherwise({ redirectTo: '/' });
})
