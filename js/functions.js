// set first section to window height
$(document).ready(function() {
    var height = $(window).height() - $('#header').outerHeight() - $('.explainer:first').outerHeight();
    $('.section').each(function() {
        $(this).css('height', height + 'px');
    });
});

// smoothly scroll to an element
$(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

// toggle menu
$(document).ready(function() {
    $('.toggle-menu').click(function() {
        $('.menu').slideToggle().css({
            'float': 'none',
            'text-align': 'right'
        });
        $('nav.menu > li').css({
            'display': 'block',
            'margin-right': 0,
            'margin-top': '.5em'
        });
    });
});

// date picker
$('.datepicker').datepicker({
   controlType: 'select',
   changeMonth: true,
   changeYear: false,
   dateFormat: 'yy-mm-dd',
   //timeFormat: 'hh:mm tt'
});
