<?php
    $title = "About";
    $image = "http://surprise-vacation.com/img/home/8.jpg";
?>
<?php include('header.php') ?>
<div class="section" style="background-image:url(img/home/1.jpg);">
    <img class="section-image" src="/img/home/1.jpg">
    <div class="section-content">
        <h1 class="no-margin black">All of the Fun,<br>None of the Stress</h1>
        <h4 class="margin-bottom-0 black">Book Your Surprise Vacation Today</h4>
        <br>
        <br>
        <a style="color:black; border-color:black; font-weight:100; margin-right:2px;" href="#2" class="button">Learn More</button>
        <a style="color:black; border-color:black; font-weight:100; margin-left:2px;" href="/store" class="button">Get Started</a>
        <br>
        <br>
    </div>
</div>
<div class="section" id="2" style="background-image:url(img/home/3.jpg); justify-content:flex-start;">
    <img class="section-image" src="/img/home/3.jpg">
    <div class="section-content" style="width:25%;">
        <h2 class="no-margin black">We'll Plan Everything</h2>
        <p class="black">From travel, to recreation, to dining and lodging, we'll book your reservations and plan your entire trip from beginning to end. The day before you leave, you can view your entire itinerary upfront or keep each activity a surprise.</p>
    </div>
</div>
<div class="section" style="background-image:url(img/home/8.jpg); justify-content:flex-end;">
    <img class="section-image" src="/img/home/8.jpg">
    <div class="section-content" style="color:white; width:33%; text-shadow:0 0 25px black;">
        <h2 class="no-margin">How Does it Work?</h2>
        <p>We already learned your vacation style through the quiz. Next you'll choose the package you want and answer a few more questions so that we can put together a free quote for you. There's no risk, and no credit card is required.</p>
        <p>Solo or family, economical or ritzy, relaxing or high adventure, we've got a vacation for everyone, tailored specifically for you.</p>
    </div>
</div>
<div class="section" style="background-image:url(img/home/9.jpg); justify-content:flex-start;">
    <img class="section-image" src="/img/home/9.jpg">
    <div class="section-content black" style="width:33%;">
        <h2 class="no-margin">Unforgettable</h2>
        <p><em>"Our surprise vacation was everything we hoped for. We had no idea where we were going to go, and that was part of the fun. The sights, the food, the beaches, the bed and breakfast, everything was perfect for our family, and the kids had a great time. We would have never thought of all this on our own." - Steve and Teresa Gashler</em></p>
    </div>
</div>
<div class="section" style="background-image:url(img/home/2.jpg); justify-content:flex-end;">
    <img class="section-image" src="/img/home/2.jpg">
    <div class="section-content" style="width:35%;">
        <h2 class="no-margin" style="color:white;">Are You Game?</h2>
        <p style="color:white;">Surprise vacations aren't for everyone. If you're in to the routine and predictable, you're probably better off planning your own vacation. But if you're up for an adventure, you've come to the right place. Planning the perfect vacation can take days of work. Plus, we can help you get better deals. So why not skip the hassle and treat yourself to an all-new experience?</p>
        <br>
        <a href="/store?type=learn" class="button" style="color:white; border-color:white;">Learn More</a>
        <a href="/store?type=start" class="button" style="color:white; border-color:white;">Get Started</a>
    </div>
</div>
<div class="section">
    <img class="section-image" src="/img/home/2.jpg">
    <div class="section-content" style="width:35%;">
        <h2 class="no-margin">Still Not Sure?</h2>
        <p>Contact one of our friendly travel agents to find out what Surprise Vacation can do for you.</p>
        <br>
        <a href="/contact" class="button">Contact Us</a>
    </div>
</div>
<?php include('footer.php') ?>
