<?php include('header.php') ?>
<div class="page">
    <h1 class="margin-top-0" style="font-size:2em;">
        <?php
        if (!isset($_GET['type']) || $_GET['type'] == 'learn') {
            echo 'Learn More About Surprise Vacations';
        } else {
            echo 'Get Started With Your Surprise Vacation';
        }
        ?>
    </h1>
    <form name="form" action="submit.php" method="post" onsubmit="return validateForm()">
        <ol>
            <li>
                How many people are in your party?
                <input type="text" name="number">
            </li>
            <li>
                When would you like to leave?
                <input type="text" name="depart">
            </li>
            <li>
                When would you like to get back?
                <input type="text" name="return">
            </li>
            <li>
                What's your maximum budget for your vacation?
                <input type="text" name="budget">
            </li>
            <li>
                Your name:
                <input type="text" name="name">
            </li>
            <li>
                Your email address:
                <input type="text" name="email">
            </li>
            <li>
                Your phone number:
                <input type="text" name="phone">
            </li>
        </ol>
        <br>
        <button>Next <i class="fa fa-angle-right"></i></button>
    </form>
</div>
<script>

    // validate
    function validateForm()
    {
        var fields = [
            'number',
            'depart',
            'return',
            'budget',
            'name',
            'phone',
            'email'
        ];
        for (i = 0; i < fields.length; i++) {
            var fieldname = fields[i];
            if (document.forms["form"][fieldname].value === "") {
                alert("Please complete the form before continuing.");
                return false;
            }
        }
        return true;
    }

</script>
<?php include('footer.php') ?>
