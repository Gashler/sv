<?php include('header.php') ?>
<div class="page">
    <h1 class="margin-top-0" style="font-size:2em;">Contact Us</h1>
    <p>Find out what Suprise Vacation can do for you. Contact one of our friendly travel agents by calling <strong>801-494-3440</strong> or by using the form below:</p>
    <form name="form" action="submit.php" method="post" onsubmit="return validateForm()">
        <ol>
            <li>
                <label>Name</label>
                <input type="text" name="name">
            </li>
            <li>
                <label>Email</label>
                <input type="text" name="email">
            </li>
            <li>
                <label>Questions/Comments</label>
                <textarea rows="5" type="text" name="body"></textarea>
            </li>
        </ol>
        <button>Submit <i class="fa fa-angle-right"></i></button>
    </form>
</div>
<script>

    // validate
    function validateForm()
    {
        var fields = [
            'name'
            'email'
            'body'
        ];
        for (i = 0; i < fields.length; i++) {
            var fieldname = fields[i];
            if (document.forms["form"][fieldname].value === "") {
                alert("Please complete the form before continuing.");
                return false;
            }
        }
        return true;
    }

</script>
<?php include('footer.php') ?>
