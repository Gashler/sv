<!doctype html>
<html ng-app="app">
    <body>
        <head>
            <title><?php echo $title ?> | Surprise Vacation</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta property="og:site_name" content="Book Your Own Surprise Vacation" />
            <meta property="og:title" content="<?php echo $title ?>" />
            <?php if (isset($image)) : ?>
                <meta property="og:image" content="<?php echo $image ?>" />
            <?php endif ?>
            <meta property="og:description" content="" />
            <meta property="og:url" content="http://surprise-vacation.com/profile.php" />

            <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="css/theme.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
            <link rel="stylesheet" src="/packages/jquery-ui/jquery-ui.theme.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>
            <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-route.min.js"></script>
            <script src="/packages/jquery-ui/jquery-ui.min.js"></script>
            <script src="/js/app.js"></script>
            <script src="/js/functions.js"></script>
            <script src="/js/controllers/survey.js"></script>
        </head>
        <body>
            <div id="header">
                <div id="menu">
                    <a href="/" id="logo">
                        <i class="fa fa-star"></i> Surprise Vacation
                    </a>
                    </a>
                    <i class="fa fa-bars float-right toggle-menu"></i>
                    <nav class="menu float-right">
                        <li><a href="/about">About</a></li>
                        <li><a href="/contact">Contact</a></li>
                    </nav>
                </div>
            </div>
