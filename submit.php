<?php
$to = "support@surprise-vacation.com";
$subject = "Surprise Vacation Contact Form";

$message = "
<html>
<head>
<title>Surprise Vacation Contact Form</title>
</head>
<body>
<table>
    <tr>
        <th style='text-align:right;'>Name</th>
        <td>" . $_POST['name'] . "</td>
    </tr>
    <tr>
        <th style='text-align:right;'>Email</th>
        <td>" . $_POST['email'] . "</td>
    </tr>
    <tr>
        <th style='text-align:right;'>Message</th>
        <td>" . $_POST['body'] . "</td>
    </tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <webmaster@surprise-vacation.com>' . "\r\n";

mail($to,$subject,$message,$headers);
header("Location: contact-confirmation.php");
