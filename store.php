<?php
    $json = file_get_contents('pricing.json');
    $pricing = json_decode($json);
    $title = 'Store';
?>
<?php include('header.php') ?>
<div class="section no-padding">
    <div class="explainer">
        <h1>Choose Your Package</h1>
        <p class="uppercase red no-margin align-center">All Packages 40% Off When You Book by <?php echo date('M tS') ?></p>
    </div>
    <?php foreach ($pricing->packages as $package_key => $package) : ?>
        <div class="division width-third height-full pricing<?php if ($package_key == 'gold') echo '-highlight' ?>">
            <!-- <div class="badge">Save<br>${{ (<?php echo $package_key ?>_price * <?php echo $pricing->markup ?>) - <?php echo $package_key ?>_price }}</div> -->
            <h2><?php echo $package->days ?> Days</h2>
            <div class="size-0-75">We'll Book/Plan Your ...</div>
            <ul class="size-0-75 inline-block align-left padding-0">
                <li>Travel</li>
                <li>Lodging</li>
                <li>Dining</li>
                <li>Activities</li>
            </ul>
            <form action="/checkout" type="post">
                <input type="hidden" name="package" value="<?php echo $package_key ?>">
                <select class="size-0-75" name="price" ng-model="<?php echo $package_key ?>_price" ng-init="<?php echo $package_key ?>_price = '<?php echo $package->prices->standard ?>'">
                    <?php foreach ($package->prices as $price_key => $price) : ?>
                        <option <?php if ($price_key == 'standard') echo 'selected' ?> value="<?php echo $price ?>"><?php echo ucfirst($price_key) ?></option>
                    <?php endforeach ?>
                </select>
                <?php
                    $index = 0;
                    foreach ($pricing->explanations as $explanation_key => $explanation) {
                        if ($index == 0) $name = 'economy';
                        if ($index == 1) $name = 'standard';
                        if ($index == 2) $name = 'luxury';
                        echo '<p class="size-0-75 align-center" ng-if="' . $package_key . '_price == ' . $package->prices->$name . '">' . $explanation . '</p>';
                        $index ++;
                    }
                ?>
                <!-- <div class="strike">
                    {{ <?php echo $package_key ?>_price * <?php echo $pricing->markup ?> | currency }}
                    <div class="line"></div>
                </div>
                <div>${{ <?php echo $package_key ?>_price }}</div> -->
                <br>
                <button>Select <i class="fa fa-angle-right"></i></button>
            </form>
        </div>
    <?php endforeach ?>
</div>
<script>
    $('ul').css('height', $('ul:last').height() + 'px');
</script>
<?php include('footer.php') ?>
