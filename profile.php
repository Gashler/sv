<?php
    session_start();
    $json = file_get_contents('data.json');
    $data = json_decode($json);

    // get primary and secondary types
    foreach ($data->types as $type) {
        if ($type->id == $_SESSION['primary']) {
            $types[0] = $type;
        }
        if ($type->id == $_SESSION['secondary']) {
            $types[1] = $type;
        }
    };
    // get location
    $location = $data->locations[$_SESSION['l']];
    if (isset($location->state)) {
        $location->full = $location->city . ', ' . $location->state;
    } else {
        $location->full = $location->city . ', ' . $location->country;
    }
    $title = "Your Ideal Vacation: $location->full";
    $image = "http://surprise-vacation.com/img/locations/$location->image";
?>
<?php include('header.php') ?>
<div class="page-wide">
    <div class="section-small display-block no-padding">
        <div class="padding-5" id="profile-location" style="position:relative; background: url(/img/locations/<?php echo $location->image ?>); background-size:cover; height:600px; background-position:center center">
            <a class="button" href="/" style="background:rgb(255,64,0); color:white; border:none; position:absolute; top:5%; right:2.5%;">Take the Quiz <i class="fa fa-angle-right"></i></a>
            <img src="/img/locations/<?php echo $location->image ?>" style="height:1px;">
            <div class="division-content">
                <h4 class="no-margin">Your Ideal Vacation:</h4>
                <h2 style="margin:0 0 .5em">
                    <?php echo $location->full ?>
                </h2>

                <!-- Facebook -->
                <div id="fb-root"></div>
                <div style="position:relative; top:-10px;" class="fb-share-button" data-layout="button">
                </div>

                <!-- Twitter -->
                <a style="vertical-align:top;" href="https://twitter.com/share" class="twitter-share-button">Tweet</a>

                <!-- Pinterest -->
                <a style="vertical-align:top;" href="https://www.pinterest.com/pin/create/button/">
                    <img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" />
                </a>

                <!-- Google Plus -->
                <div style="vertical-align:top;" class="g-plusone" data-annotation="none"></div>

            </div>
        </div>
        <div class="padding-5">

            <h4 class="margin-bottom-0">You are a:</h4>
            <h2 class="margin-top-0"><?php echo $types[0]->adjective ?> <?php echo $types[1]->person ?></h2>
            <div class="align-left">
                <p>
                    You love to <?php echo $types[0]->description ?>.
                    <?php if ($types[0]->id !== $types[1]->id) : ?>
                        You also like to <?php echo $types[1]->description ?>.
                    <?php endif ?>
                </p>
                <p>Enjoy somewhere <?php echo $types[0]->activities_adjective ?> for activities such as <?php echo $types[0]->activities[0] ?> and <?php echo $types[0]->activities[1] ?>. Eat at <?php echo $types[0]->dining_adjective ?> locations with options such as <?php echo $types[0]->dining[0] ?> and <?php echo $types[0]->dining[1] ?>. In the evenings, try <?php echo $types[0]->entertainment_adjective ?> entertainment such as <?php echo $types[0]->entertainment[0] ?> or <?php echo $types[0]->entertainment[1] ?>. You should look for <?php echo $types[0]->dining_adjective ?> lodging such as <?php echo $types[0]->lodging[0] ?> or <?php echo $types[0]->lodging[1] ?>.</p>
                <p><?php echo $location->description ?></p>
            </div>
        </div>
        <div class="padding-5" style="background:rgb(245,245,245);">
            <div>
                <!-- <p class="margin-bottom-0 uppercase size-1-5">Thanks for Taking Our Survey!</p>
                <h2 class="no-margin red">Save 40% on a Vacation Package</h2>
                <p class="margin-top-0 uppercase size-1-5">When You Sign Up by <?php echo date('M tS') ?></p> -->
                <p class="margin-bottom-0 uppercase size-1-5">Thanks for Taking Our Survey!</p>
                <h2 class="no-margin blue">Save 40% on a Vacation Package</h2>
                <p class="margin-top-0 uppercase size-1-5">No Credit Card Required</p>
                <img src="/img/strip.jpg" alt="Vacation Ideas" width="877" height="150" style="width:100%; max-width:877px;">
                <br>
                <br>
                <a class="button" href="/store">Packages for <?php echo $location->city ?> <i class="fa fa-angle-right"></i></a>
                <a class="button" href="/about">Try a Surprise Vacation <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>
<script src="/js/controllers/profile.js"></script>
<script>

    // facebook button
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // twitter button
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

</script>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<?php include('footer.php') ?>
